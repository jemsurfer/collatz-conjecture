# The Collatz Conjecture, or 3x+1
This is a program to practice my rust skills and demonstrate an interesting maths problem called the Collatz Conjecture.<br>
## Explanation
The collatz conjecture is a maths problem with 3 simple rules:<br>
- If the number is odd, multiply by 3 and add 1.
- If the number is even, divide by 2.<br>
- Once the number reaches 1, stop, because 1 becomes 4, which becomes 2, which becomes 1.
